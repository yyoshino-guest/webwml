#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
<define-tag pagetitle>Debian 10 更新：10.2 发布</define-tag>
<define-tag release_date>2019-11-16</define-tag>
<define-tag frontpage>yes</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian 项目很高兴地宣布对 Debian <release> 稳定版的第二次更新（发行版代号 \
<q><codename></q>）。此次小版本更新主要添加了对安全问题的修正补丁，以及为一些严重问题\
所作的调整。安全通告已单独发布，并会在适当的情况下予以引用。</p>

<p>请注意，此更新并不是 Debian <release> 的新版本，它仅更新了所包含的一些软件包。\
没有必要丢弃旧的<q><codename></q>的安装介质。在安装之后，只需使用最新的 Debian \
镜像更新旧的软件包即可。</p>

<p>经常从 security.debian.org 安装更新的用户将不必更新许多软件包，\
因本更新中包含了 security.debian.org 的大多数更新。</p>

<p>新的安装镜像即将于常规的位置予以提供。</p>

<p>只需令软件包管理系统指向 Debian 的许多 HTTP 镜像站点之一，\
您便能够把已有的系统升级至本次更新版本。详尽的镜像列表可以在以下网址处获得：</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>杂项错误修正</h2>

<p>此稳定版更新为以下软件包添加了一些重要的修正：</p>

<table border=0>
<tr><th>软件包</th>               <th>原因</th></tr>
<correction aegisub "Fix crash when selecting a language from the bottom of the <q>Spell checker language</q> list; fix crash when right-clicking in the subtitles text box">
<correction akonadi "Fix various crashes / deadlock issues">
<correction base-files "Update /etc/debian_version for the point release">
<correction capistrano "Fix failure to remove old releases when there were too many">
<correction cron "停止使用过时的 SELinux API">
<correction cyrus-imapd "Fix data loss on upgrade from version 3.0.0 or earlier">
<correction debian-edu-config "Handle newer Firefox ESR configuration files; add post-up stanza to /etc/network/interfaces eth0 entry conditionally">
<correction debian-installer "修复使用 EFI 引导的 netboot 映像时在 hidpi 显示器上出现的不可读字体">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction distro-info-data "Add Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter "新上游稳定释出版本； fix sysvinit support; catch more ASCII encoding errors to improve resilience against bad data; fix message extraction so that signing in the same pass through the milter as verifying works correctly">
<correction emacs "Update the EPLA packaging key">
<correction fence-agents "Fix incomplete removal of fence_amt_ws">
<correction flatpak "新上游稳定释出版本">
<correction flightcrew "Security fixes [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "修复中文环境下在现代浏览器中 Noto CJK 字体的过于激进的字体选择问题">
<correction freetype "Properly handle phantom points for variable hinted fonts">
<correction gdb "Rebuild against new libbabeltrace, with higher version number to avoid conflict with earlier upload">
<correction glib2.0 "Ensure libdbus clients can authenticate with a GDBusServer like the one in ibus">
<correction gnome-shell "新上游稳定释出版本； fix truncation of long messages in Shell-modal dialogs; avoid crash on reallocation of dead actors">
<correction gnome-sound-recorder "Fix crash when selecting a recording">
<correction gnustep-base "Disable gdomap daemon that was accidentally enabled on upgrades from stretch">
<correction graphite-web "Remove unused <q>send_email</q> function [CVE-2017-18638]; avoid hourly error in cron when there is no whisper database">
<correction inn2 "Fix negotiation of DHE ciphersuites">
<correction libapache-mod-auth-kerb "Fix use after free bug leading to crash">
<correction libdate-holidays-de-perl "Mark International Childrens Day (Sep 20th) as a holiday in Thuringia from 2019 onwards">
<correction libdatetime-timezone-perl "Update included data">
<correction libofx "Fix null pointer dereference issue [CVE-2019-9656]">
<correction libreoffice "Fix the postgresql driver with PostgreSQL 12">
<correction libsixel "Fix several security issues [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Fix dangling pointer in xsltCopyText [CVE-2019-18197]">
<correction lucene-solr "Disable obsolete call to ContextHandler in solr-jetty9.xml; fix Jetty permissions on SOLR index">
<correction mariadb-10.3 "新上游稳定释出版本">
<correction modsecurity-crs "Fix PHP script upload rules [CVE-2019-13464]">
<correction mutter "新上游稳定释出版本">
<correction ncurses "Fix several security issues [CVE-2019-17594 CVE-2019-17595] and other issues in tic">
<correction ndppd "Avoid world writable PID file, that was breaking daemon init scripts">
<correction network-manager "Fix file permissions for <q>/var/lib/NetworkManager/secret_key</q> and /var/lib/NetworkManager">
<correction node-fstream "Fix arbitrary file overwrite issue [CVE-2019-13173]">
<correction node-set-value "Fix prototype pollution [CVE-2019-10747]">
<correction node-yarnpkg "Force using HTTPS for regular registries">
<correction nx-libs "Fix regressions introduced in previous upload, affecting x2go">
<correction open-vm-tools "Fix memory leaks and error handling">
<correction openvswitch "Update debian/ifupdown.sh to allow setting-up the MTU; fix Python dependencies to use Python 3">
<correction picard "Update translations to fix crash with Spanish locale">
<correction plasma-applet-redshift-control "Fix manual mode when used with redshift versions above 1.12">
<correction postfix "新上游稳定释出版本； work around poor TCP loopback performance">
<correction python-cryptography "Fix test suite failures when built against newer OpenSSL versions; fix a memory leak triggerable when parsing x509 certificate extensions like AIA">
<correction python-flask-rdf "Add Depends on python{3,}-rdflib">
<correction python-oslo.messaging "新上游稳定释出版本； fix switch connection destination when a rabbitmq cluster node disappears">
<correction python-werkzeug "Ensure Docker containers have unique debugger PINs [CVE-2019-14806]">
<correction python2.7 "Fix several security issues [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Fix rpc.rquotad spinning at 100% CPU">
<correction rpcbind "Allow remote calls to be enabled at run-time">
<correction shelldap "Repair SASL authentications, add a 'sasluser' option">
<correction sogo "Fix display of PGP-signed e-mails">
<correction spf-engine "新上游稳定释出版本； fix sysvinit support">
<correction standardskriver "Fix deprecation warning from config.RawConfigParser; use external <q>ip</q> command rather than deprecated <q>ifconfig</q> command">
<correction swi-prolog "Use HTTPS when contacting upstream pack servers">
<correction systemd "core: never propagate reload failure to service result; fix sync_file_range failures in nspawn containers on arm, ppc; fix RootDirectory not working when used in combination with User; ensure that access controls on systemd-resolved's D-Bus interface are enforced correctly [CVE-2019-15718]; fix StopWhenUnneeded=true for mount units; make MountFlags=shared work again">
<correction tmpreaper "Prevent breaking of systemd services that use PrivateTmp=true">
<correction trapperkeeper-webserver-jetty9-clojure "Restore SSL compatibility with newer Jetty versions">
<correction tzdata "新上游发行版本">
<correction ublock-origin "新上游版本，与 Firefox ESR68 兼容">
<correction uim "Resurrect libuim-data as a transitional package, fixing some issues after upgrades to buster">
<correction vanguards "新上游稳定释出版本； prevent a reload of tor's configuration via SIGHUP causing a denial-of-service for vanguards protections">
</table>


<h2>安全更新</h2>


<p>此修订版本将以下安全更新添加到了稳定发行版本中。安全团队已经分别为这些更新发布了通告：</p>

<table border=0>
<tr><th>通告编号</th>  <th>软件包</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>删除的软件包</h2>

<p>由于我们无法控制的情况，以下软件包已被删除：</p>

<table border=0>
<tr><th>软件包</th>               <th>原因</th></tr>
<correction firefox-esr "[armel] 由于 nodejs 构建依赖的缘故不再受支持">

</table>

<h2>Debian 安装器</h2>
<p>安装器已经更新，以配合发布时包含在稳定版本中的修正内容。</p>

<h2>链接</h2>

<p>此修订版本中有更改的软件包的完整列表：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>当前稳定发行版：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>拟议的稳定发行版更新：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>稳定发行版信息（发行说明，勘误等）：</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>安全公告及信息：</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>关于 Debian</h2>

<p>Debian 项目是一个自由软件开发者组织，这些志愿者为制作完全自由免费的 Debian 操作系统\
而自愿贡献时间和精力。</p>


<h2>联系信息</h2>

<p>更多信息，请访问 Debian 主页
<a href="$(HOME)/">https://www.debian.org/</a>、发送邮件至
&lt;press@debian.org&gt; ，或联系稳定版本发布团队
&lt;debian-release@lists.debian.org&gt;。</p>


