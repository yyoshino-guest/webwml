#use wml::debian::translation-check translation="4f5ef50b5d7008577cb1752c366cedd8f01980e3" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8553">CVE-2015-8553</a>

    <p>Jan Beulich opdagede at 
    <a href="https://security-tracker.debian.org/tracker/CVE-2015-2150">\
    CVE-2015-2150</a> ikke var løst fuldstændigt.  Hvis en fysisk PCI-funktion 
    blev overført til en Xen-gæst, kunne gæsten var i stand til at tilgå dens 
    hukommelse og I/O-områder, før dekodning af disse områder blev aktiveret.  
    Det kunne medføre et lammelsesangreb (uventet NMI) på værten.</p>

    <p>Rettelsen af denne fejl er ikke kompatibel med versioner af qemu før 
    2.5.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18509">CVE-2017-18509</a>

    <p>Denis Andzakovic rapporterede om en manglende typekontrol i 
    implementeringen af IPv4 multicast-routing.  En bruger med muligheden 
    CAP_NET_ADMIN (i et vilkårlig brugernavnerum) kunne udnytte fejlen til 
    lammelsesangreb (hukommelseskorruption eller nedbrud) eller måske til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

    <p>ADLab fra VenusTech opdagede at kernen loggede den virtuelle adresse, som 
    tildeles pr.-CPU-data, hvilket kunne gøre det lettere at udnytte andre 
    sårbarheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang rapporterede om en kapløbstilstand i libsas, kerneundersystemet 
    som understøtter Serial Attached SCSI-enheder (SAS), hvilket kunne føre til 
    en anvendelse efter frigivelse.  Det står ikke klart hvordan det kan 
    udnyttes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20856">CVE-2018-20856</a>

    <p>Xiao Jin rapporterede om en potentiel dobbelt frigivelse i 
    block-undersystemet, i tilfælde af en fejl opstod mens en I/O-scheduler 
    blev initialiseret for en blockenhed.  Det står ikke klart hvorvidt denne 
    fejl kan udnyttes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>Man opdagede at de fleste x86-processorer kunne spekulativt springe over 
    en betinget SWAPGS-instruktion, som anvendes når man går ind i kernen fra 
    brugertilstand, og/eller kunne spekulativt udføre den, når den skulle 
    springes over.  Det er en undertype af Spectre variant 1, som kunne gøre det 
    muligt for lokale brugere at få adgang til følsomme oplysninger fra kernen 
    eller andre processer.  Det er løst ved at anvende hukommelsesbarrierer til 
    at begrænse spekulativ udførelse.  Systemer, der anvender en i386-kerne, er 
    ikke påvirket, da kernen ikke anvender SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

    <p>Man opdagede at implementeringen af vfio ikke begrænsede antallet af 
    DMA-mapninger til enhedshukommelse.  En lokal bruger, der har fået ejerskab 
    af en vfio-enhed, kunne udnytte fejlen til at forårsage et lammelsesangreb 
    (ikke mere hukommelse-tilstand).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3900">CVE-2019-3900</a>

    <p>Man opdagede at vhost-driverne ikke på korrekt vis kontrollerede mængden 
    af arbejde der blev gjort for at betjene forespørgsler fra gæste-VM'er.  En 
    ondsindet gæst kunne udnytte fejlen til at forårsage et lammelsesangreb
    (ubegrænset CPU-forbrug) på værten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>Værktøjet syzkaller fandt en potentiel nulldereference i forskellige 
    drivere til UART-tilsluttede Bluetooth-adaptere.  En lokal bruger med 
    adgang til en pty-enhed eller andre passende tty-enheder, kunne anvende 
    fejlen til et lammelsesangreb (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein og Benny Pinkas opdagede at genereringen af IP-pakke-id'er 
    anvendte en svag hash-funktion, <q>jhash</q>.  Det kunne medføre sporing af 
    individuelle computere, når de kommunikerer med forskellige fjerne servere 
    og fra forskellige netværk.  I stedet anvendes nu funktionen 
    <q>siphash</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

    <p>Amit Klein og Benny Pinkas opdagede at genereringen af IP-pakke-id'er 
    anvendte en svag hash-funktion, der integrerede en virtuel kerneadresse.  
    Denne hash-funktion anvendes ikke længere IP-id'er, selv om den stadig 
    anvendes til andre formål i netværksstakken.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>Man opdagede at gtco-driveren til USB-inputtablets kunne overløbe en 
    stakbuffer med konstante data, mens en enheds descriptor blev fortolket.  En 
    fysisk tilstedeværende bruger med en særligt fremstillet USB-enhed, kunne 
    udnytte fejlen til at forårsage et lammelsesangreb (BUG/oops) eller måske 
    til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey rapporterede at på PowerPC-systemer (ppc64el) uden 
    Transactional Memory (TM), forsøgte kernen alligevel at gendanne TM-state 
    overført til systemkaldet sigreturn() system call.  En lokal bruger kunne 
    udnytte fejlen til lammelsesangreb (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>Værktøjet syzkaller fandt en manglende grænsekontrol i diskettedriveren.  
    En lokal bruger med adgang til en disketteenhed, uden en diskette, kunne 
    anvende fejlen til at læse kernehukommelse ud over I/O-bufferen, og måske 
    få fat i følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>Værktøjet syzkaller fandt en potentiel division med nul i 
    diskettedriveren.  En lokal bruger med adgang til en disketteenhed, kunne 
    anvende fejlen til lammelsesangreb (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15239">CVE-2019-15239</a>

    <p>Denis Andzakovic rapporterede om en mulig anvendelse efter frigivelse i 
    implementeringen af TCP-sockets.  En lokal bruger kunne udnytte fejlen til 
    lammelsesangreb (hukommelseskorruption eller nedbrud) eller måske til 
    rettighedsforøgelse.</p></li>

<li>(CVE-id endnu ikke tildelt)

    <p>Undersystemet netfilter-conntrack anvendte kerneadresser som id'er 
    synlige for brugerne, hvilket kunne gøre det lettere at udnytte andre 
    sikkerhedssårbarheder.</p></li>

<li>XSA-300

    <p>Julien Grall rapporterede at Linux ikke begrænsede mængden af hukommelse, 
    som et domæne forsøger at udvide, der var heller ikke begrænsning af mængden 
    af <q>foreign/grant map</q>-hukommelse, som hver enkelt gæst kan forbruge, 
    førende til lammelsesangrebstilstande (for værter eller gæster).</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 4.9.168-1+deb9u5.</p>

<p>I den stabile distribution (buster), er disse problemer primært løst 
i version 4.19.37-5+deb10u2 eller tidligere.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4497.data"
