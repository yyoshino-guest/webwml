#use wml::debian::translation-check translation="cc173b8d34b89c7d43e8628759e88ae4a67b7db9" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Simon McVittie opdagede en fejl i servicen flatpak-portal, som kunne gøre det 
muligt for applikationer i sandkasser at udføre vilkårlig kode på værtssystemet 
(undslippelse fra sandkassen).</p>

<p>Flatpak-portalens D-Bus-service (flatpak-portal, også kendt under sit 
D-Bus-servicenavn org.freedesktop.portal.Flatpak), tillod at apps i en 
Flatpak-sandkasse til at starte deres egne underprocesser i en ny 
sandkasseinstans, enten med de samme sikkerhedsindstillinger som den kaldende 
eller med mere restriktive sikkerhedsindstillinger.  Eksempelvis anvendes det i 
Flatpak-pakkede webbrowsere så som Chromium, til at starte underprocesser som 
vil behandle webindhold der ikke er tillid til, og give disse underprocesser en 
mere restriktiv sandkasse end browseren selv.</p>

<p>I sårbare versioner overførte Flatpak-portalservicen miljøvariabler, angivet 
af den kaldende, til processer som ikke er i sandkasser på værtssystemet, og i 
særdeleshed til flatpaks run-kommando, som anvendes til at starte den nye 
sandkasseinstans.  En ondsindet eller komprimitteret Flatpak-app kunne opsætte 
miljøvariabler, som flatpaks run-kommando har tillid til, og anvendes dem til at 
udføre vilkårlig kode, som ikke er i en sandkasse.</p>

<p>I den stabile distribution (buster), er dette problem rettet i version 
1.2.5-0+deb10u2.</p>

<p>Vi anbefaler at du opgraderer dine flatpak-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende flatpak, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4830.data"
