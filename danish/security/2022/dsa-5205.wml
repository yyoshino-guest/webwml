#use wml::debian::translation-check translation="cbe9cd7fab7b8b8992562b1757c8d4d429b5c67c" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Samba, en SMB/CIFS-fil-, -print- og 
loginserver til Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2031">CVE-2022-2031</a>

    <p>Luke Howard rapporterede at Samba AD-brugere kunne omgå visse 
    begrænsninger i forbindelse med ændring af adgangskoder.  En bruger, der er 
    blevet bedt om at skifte sin adgangskode, kunne udnytte fejlen til at få fat 
    i og anvende ticket til andre services.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32742">CVE-2022-32742</a>

    <p>Luca Moro rapporterede at en SMB1-klient med skriveadgang til et share, 
    kunne forårsage at serverhukommelsesindhold kunne lækkes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32744">CVE-2022-32744</a>

    <p>Joseph Sutton rapporterede at Samba AD-brugere kunne fabrikere 
    adgangskodeændringsbeskeder for enhver bruger, medførende 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>

    <p>Joseph Sutton rapporterede at Samba AD-brugere kunne få serverprocessen 
    til at gå ned ved hjælp af en særligt fremstillet tilføjelses- eller 
    ændringsforespørgsel i LDAP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32746">CVE-2022-32746</a>

    <p>Joseph Sutton og Andrew Bartlett rapporterede at Samba AD-brugere kunne 
    forårsage en anvendelse efter frigivelse i serverprocessen, med en særligt 
    fremstillet tilføjelses- eller ændringsforespørgsel til LDAP.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 2:4.13.13+dfsg-1~deb11u5.  Rettelsen af 
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">\
CVE-2022-32745</a>, krævede en opdatering af ldb 2:2.2.3-2~deb11u2, for at rette 
fejlen.</p>

<p>Vi anbefaler at du opgraderer dine samba-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende samba, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5205.data"
