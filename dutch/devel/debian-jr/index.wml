#use wml::debian::template title="Het project Debian Jr."
#use wml::debian::translation-check translation="1df7fb6f20f7d03e33cbc3ede19249bfa322abe2"

<h2>Debian voor kinderen</h2>

<p>
Dit is een zogenaamde <a href="https://www.debian.org/blends/">Debian Pure
Blend</a> (Blend in het kort), d.w.z. een doelgroepspecifieke uitgave van
Debian. Onze doelstelling is om van Debian een besturingssysteem te maken dat
kinderen van alle leeftijden graag gebruiken. In eerste instantie zullen wij
ons richten op de productie van iets voor kinderen tot 8 jaar. Zodra we dit
hebben bereikt, is onze volgende doelgroep 7 tot 12 jaar. Tegen de tijd dat
kinderen hun tienerjaren bereiken, zouden ze vertrouwd moeten zijn met het
gebruik van Debian zonder speciale aanpassingen.
</p>

<h3>E-maillijst</h3>
<p>
Debian heeft een e-maillijst opgezet voor deze groep.  U kunt zich
<a href="https://lists.debian.org/debian-jr/">
erop abonneren of de e-mailarchieven lezen</a>.
</p>

<h3>Chat</h3>
<p>
We hebben een realtime discussiekanaal, #debian-jr op irc.debian.org en een
XMPP-chat voor meerdere gebruikers
<a href="xmpp:debian-jr@conference.debian.org?join">
debian-jr@conference.debian.org</a>.
</p>

<h3>Nieuws</h3>
<p>
We hebben een
<a href="https://debian-jr-team.pages.debian.net/blog/">Teamblog</a>
voor projectnieuws en updates.
</p>

<h3>Gemeenschapssite</h3>
<p>
We hebben een
<a href="https://wiki.debian.org/Teams/DebianJr">Debian Jr.
gemeenschapssite</a> waar ontwikkelaars en gebruikers worden aangemoedigd om
bij te dragen aan het project Debian Jr.
</p>

<h3>Wat kan ik doen om te helpen?</h3>
<p>
We zouden graag willen horen wat u denkt dat Debian Jr. zou kunnen doen,
vooral als u zou willen helpen om dit te realiseren.
</p>

<h3>Installatie</h3>
<p>
Het pakket <a href="https://packages.debian.org/junior-doc">junior-doc</a>
bevat de beknopte handleiding.
</p>

<h3>Pakketten in Debian Jr.</h3>

<p>
Debian Pure Blends maakt een <a href="https://blends.debian.org/junior/tasks/">overzicht van de pakketten</a> die interessant zijn voor de doelgroep.
</p>


<h3>Doelstellingen van Debian Jr.</h3>

<h4>Debian aantrekkelijk maken voor kinderen</h4>

<p>
Het primaire doel van het Debian Jr.-project is om van Debian een
besturingssysteem te maken dat onze kinderen <i>willen</i> gebruiken. Dit houdt
een zekere gevoeligheid in voor de behoeften van kinderen zoals die door de
kinderen zelf worden uitgedrukt. Als ouders, ontwikkelaars, oudere broers en
zussen en systeembeheerders moeten we onze oren en ogen openhouden en ontdekken
wat het is dat computers aantrekkelijk maakt voor kinderen. Zonder deze focus
kunnen we gemakkelijk afglijden naar abstracte doelen zoals
"gebruiksvriendelijkheid", "eenvoud", "weinig onderhoud" of "robuustheid" die,
hoewel ze zeker prijzenswaardig zijn voor Debian als geheel, te breed zijn om
tegemoet te komen aan de specifieke behoeften en wensen van kinderen.
</p>

<h4>Toepassingen</h4>

<p>
Natuurlijk hebben kinderen andere behoeften en wensen dan volwassenen wat
betreft de toepassingen die zij willen gebruiken. Sommige daarvan zijn
spelletjes, terwijl andere tekstverwerkers, teksteditors, "verf"programma's en
dergelijke zullen zijn. Het doel is om de beste toepassingen te identificeren
die binnen Debian beschikbaar zijn en geschikt zijn voor kinderen, dat aantal
aan te vullen met toepassingen die nog niet in Debian beschikbaar zijn, en
ervoor te zorgen dat de gekozen toepassingen goed onderhouden worden. Eén
implementatiedoel is het aanbieden van metapakketten om het installeren van
groepen "kindvriendelijke" toepassingen gemakkelijker te maken voor de
systeembeheerder. Een ander is het verbeteren van onze pakketten op een manier
die er vooral toe doet voor kinderen, wat zo eenvoudig kan zijn als het
opvullen van gaten in de documentatie, of complexer kan zijn, waarbij we
samenwerken met de bovenstroomse auteurs.
</p>

<h4>Kindveilig maken en accountbeheer</h4>

<p>
Het is niet de bedoeling om per se strenge beveiligingsmaatregelen te nemen.
Dat valt buiten ons mandaat. Het doel is eenvoudigweg om systeembeheerders
documentatie en hulpmiddelen te bieden om hun systemen zo in te stellen dat hun
van nature nieuwsgierige kinderen-gebruikers hun accounts niet "onklaar maken",
niet alle systeembronnen opslokken, of andere dingen doen die een voortdurend
ingrijpen van de systeembeheerder vereisen. Dit is meer een probleem bij
kinderen dan bij volwassenen, omdat zij de neiging hebben om het systeem te
verkennen en opzettelijk tot het uiterste te drijven om te zien wat er gebeurt.
De rotzooi die daarbij ontstaat kan zowel amusant als frustrerend zijn. Deze
doelstelling gaat over het bewaren van je mentale gezondheid (en gevoel voor
humor) als systeembeheerder van een kind.
</p>

<h4>De computer leren gebruiken</h4>

<p>
De doelstelling "kindveiligheid" moet worden afgewogen tegen de doelstelling
dat het kinderen <i>toegestaan is</i> om dingen uit te proberen (en ja, onklaar
te maken) en oplossingen voor hun problemen te zoeken. Het leren gebruiken van
het toetsenbord, de grafische gebruikersinterface, de shell en computertalen
zijn allemaal dingen waarbij zowel ouders als kinderen wat tips kunnen
gebruiken om hen in de goede richting op weg te helpen.
</p>

<h4>Gebruikersinterface</h4>

<p>
Zowel grafische gebruikersinterfaces als op tekst gebaseerde interfaces die
goed werken voor en aantrekkelijk zijn voor kinderen ontdekken en
implementeren.  Het idee is niet om de gebruikersinterface opnieuw uit te
vinden, maar om waarde toe te voegen aan bestaande hulpmiddelen en pakketten
(vensterbeheerders, menusysteem, enzovoort) door enkele handige
voorgeselecteerde configuraties aan te bieden die volgens ons het beste werken
voor kinderen.
</p>

<h4>Begeleiding van het gezin</h4>

<p>
Ouders (en in sommige gevallen oudere broers en zussen) de hulpmiddelen geven
om hun kinderen (of broers en zussen) te helpen de computer te leren kennen, om
redelijke grenzen te stellen aan de toegang ertoe, en hen te begeleiden naar
een onafhankelijk gebruik van de computer naarmate ze ouder worden. Veel ouders
zullen zich bijvoorbeeld zorgen maken over het reguleren van het
internetgebruik om hun kinderen te beschermen totdat zij een geschikte leeftijd
hebben bereikt om met voor volwassenen bedoelde inhoud om te gaan. Het
belangrijkste om te onthouden is dat het de ouders zijn die zullen kiezen voor
wat volgens hen het beste is voor hun kinderen. De Debian Jr.-groep maakt deze
inschatting niet, maar is er om de hulpmiddelen en documentatie te bieden om de
ouders te helpen bij deze beslissingen. Dat gezegd zijnde, denk ik dat dit doel
meer gericht moet zijn op het aspect "begeleiding" dan op beperking, aangezien
het eerste een positieve activiteit is en het tweede negatief.
</p>

<h4>Systeem voor kinderen</h4>

<p>
Hoewel ons eerste doel als systeembeheerders voor kinderen waarschijnlijk zal
zijn om de kinderen accounts op onze eigen systemen te geven en deze te
voorzien van toepassingen die ze leuk vinden, komt er een tijd dat we overwegen
om ze hun eigen systeem te geven. De meest ambitieuze verwezenlijking van deze
doelstelling zou een Debian software-equivalent kunnen zijn van de
"speelgoed"computers die momenteel op de markt zijn: felgekleurde, met stickers
beklede systemen waarop vooraf software is geladen die aantrekkelijk is voor
kinderen van een bepaalde leeftijdscategorie. Het is belangrijk om in het
achterhoofd te houden dat dit nog steeds een Debian-systeem zou zijn, niet een
of andere afsplitsing van de Debian-distributie. Via het Debian
pakketbeheersysteem (via metapakketten, bijvoorbeeld) is dit een perfect
haalbaar doel en er zou in de ontwikkeling geen afsplitsing nodig moeten zijn
om een speciale Debian "kindereditie" te produceren.
</p>

<h4>Internationalisering</h4>

<p>
Engels mag dan wel de "universele" taal zijn, maar niet alle kinderen spreken
het als hun moedertaal. En hoewel internationalisering een doel van Debian zelf
zou moeten zijn, worden taalproblemen versterkt wanneer men met kinderen te
maken heeft. Kinderen zullen Debian niet willen gebruiken als er geen
ondersteuning voor hun taal is, en zullen het prettiger vinden om andere
besturingssystemen te gebruiken waar de taalondersteuning beter is. Hier moeten
we dus wel rekening mee houden.
</p>
