#use wml::debian::template title="Debian 7 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="e673bc1c67aefd757c294a7f38eb9aa9f501bd2e"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>
<toc-add-entry name="security">Veiligheidsproblemen</toc-add-entry>

<p>Het Debian-beveiligingsteam werkt in de stabiele release de pakketten bij
waarin het problemen in verband met de veiligheid heeft geïdentificeerd.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina’s</a> voor
informatie over vastgestelde beveiligingsproblemen in <q>Wheezy</q>.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan
<tt>/etc/apt/sources.list</tt> om toegang te hebben tot de laatste
beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ wheezy/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Over het algemeen worden deze
aangeduid als tussenreleases.</p>

<ul>
  <li>De eerste tussenrelease, 7.1, werd uitgebracht op
      <a href="$(HOME)/News/2013/20130615">15 juni 2013</a>.</li>
  <li>De tweede tussenrelease, 7.2, werd uitgebracht op
      <a href="$(HOME)/News/2013/20131012">12 oktober 2013</a>.</li>
  <li>De derde tussenrelease, 7.3, werd uitgebracht op
      <a href="$(HOME)/News/2013/20131214">14 december 2013</a>.</li>
  <li>De vierde tussenrelease, 7.4, werd uitgebracht op
      <a href="$(HOME)/News/2014/20140208">8 februari 2014</a>.</li>
  <li>De vijfde tussenrelease, 7.5, werd uitgebracht op
      <a href="$(HOME)/News/2014/20140426">26 april 2014</a>.</li>
  <li>De zesde tussenrelease, 7.6, werd uitgebracht op
      <a href="$(HOME)/News/2014/20140712">12 juli 2014</a>.</li>
  <li>De zevende tussenrelease, 7.7, werd uitgebracht op
      <a href="$(HOME)/News/2014/20141018">18 oktober 2014</a>.</li>
  <li>De achtste tussenrelease, 7.8, werd uitgebracht op
      <a href="$(HOME)/News/2015/20150110">10 januari 2015</a>.</li>
  <li>De negende tussenrelease, 7.9, werd uitgebracht op
      <a href="$(HOME)/News/2015/2015090502">5 september 2015</a>.</li>
  <li>De tiende tussenrelease 7.10 werd uitgebracht op
      <a href="$(HOME)/News/2016/2016040202">2 april 2016</a>.</li>
  <li>De elfde tussenrelease 7.11 werd uitgebracht op
      <a href="$(HOME)/News/2016/2016060402">4 juni 2016</a>.</li>
</ul>

<ifeq <current_release_wheezy> 7.0 "

<p>>Er zijn nog geen tussenreleases voor Debian 7.</p>" "

<p>Raadpleeg de <a
href=http://http.us.debian.org/debian/dists/wheezy/ChangeLog>\
ChangeLog</a>
voor details over de wijzigingen tussen 7.0 en <current_release_wheezy/>.</p>"/>


<p>Probleemoplossingen voor de uitgebrachte stabiele distributie gaan dikwijls
door een uitgebreide testperiode voordat ze in het archief worden aanvaard.
Nochtans zijn deze probleemoplossingen beschikbaar in de map
<a href="http://ftp.debian.org/debian/dists/wheezy-proposed-updates/">\
dists/wheezy-proposed-updates</a> van elke Debian-archiefspiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 7
  deb http://ftp.us.debian.org/debian wheezy-proposed-updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Zie voor informatie over errata en updates van het installatiesysteem
de pagina met <a href="debian-installer/">installatie-informatie</a>.
</p>
