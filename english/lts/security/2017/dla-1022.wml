<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities have been discovered in the libtiff library and the
included tools, which may result in denial of service or the execution
of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9936">CVE-2017-9936</a>

    <p>A crafted TIFF document can lead to a memory leak resulting in a
    remote denial of service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10688">CVE-2017-10688</a>

    <p>A crafted input will lead to a remote denial of service attack.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u15.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1022.data"
# $Id: $
