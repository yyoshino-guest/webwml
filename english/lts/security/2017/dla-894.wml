<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jann Horn of Google discovered a time-of-check, time-of-use race
condition in Samba, a SMB/CIFS file, print, and login server for Unix. A
malicious client can take advantage of this flaw by exploting a symlink
race to access areas of the server file system not exported under a
share definition.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.6.6-6+deb7u12.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-894.data"
# $Id: $
