<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two vulnerabilities in libextractor,
a library to obtain metadata from files of arbitrary type.</p>

<ul>
  <li>A stack-based buffer overflow in unzip.c. (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14346">CVE-2018-14346</a>)</li>

  <li>An infinite loop vulnerability in mpeg_extractor.c. (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14347">CVE-2018-14347</a>)</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these issues have been fixed in libextractor
version 1:1.3-2+deb8u2.</p>

<p>We recommend that you upgrade your libextractor packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1478.data"
# $Id: $
