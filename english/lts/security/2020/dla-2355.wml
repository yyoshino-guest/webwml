<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in bind9, an Internet Domain Name Server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

      <p>Crafted responses to TSIG-signed requests could lead to an assertion
      failure, causing the server to exit. This could be done by malicious
      server operators or guessing attackers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

      <p>An assertions failure, causing the server to exit, can be exploited
      by a query for an RSA signed zone.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1:9.10.3.dfsg.P4-12.3+deb9u7.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2355.data"
# $Id: $
