<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An arbitrary-file-write vulnerability was discovered in gzip, which
provides GNU compression utilities.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.6-5+deb9u1.</p>

<p>We recommend that you upgrade your gzip packages.</p>

<p>For the detailed security status of gzip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gzip">https://security-tracker.debian.org/tracker/gzip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2976.data"
# $Id: $
