<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in glib2.0, a general-purpose
utility library for the GNOME environment.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27218">CVE-2021-27218</a>

  <p>If g_byte_array_new_take() was called with a buffer of 4GB or more on a
  64-bit platform, the length would be truncated modulo 2**32, causing
  unintended length truncation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27219">CVE-2021-27219</a>

  <p>The function g_bytes_new has an integer overflow on 64-bit platforms due to
  an implicit cast from 64 bits to 32 bits. The overflow could potentially
  lead to memory corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28153">CVE-2021-28153</a>

  <p>When g_file_replace() is used with G_FILE_CREATE_REPLACE_DESTINATION to
  replace a path that is a dangling symlink, it incorrectly also creates the
  target of the symlink as an empty file, which could conceivably have
  security relevance if the symlink is attacker-controlled. (If the path is
  a symlink to a file that already exists, then the contents of that file
  correctly remain unchanged.)</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.50.3-2+deb9u3.</p>

<p>We recommend that you upgrade your glib2.0 packages.</p>

<p>For the detailed security status of glib2.0 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/glib2.0">https://security-tracker.debian.org/tracker/glib2.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3044.data"
# $Id: $
