<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in libde265, an open source implementation of the
h.265 video codec, which may result in denial of or have unspecified other
impact.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21599">CVE-2020-21599</a>

    <p>libde265 v1.0.4 contains a heap buffer overflow in the
    de265_image::available_zscan function, which can be exploited via a crafted
    a file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-35452">CVE-2021-35452</a>

    <p>An Incorrect Access Control vulnerability exists in libde265 v1.0.8 due to
    a SEGV in slice.cc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36408">CVE-2021-36408</a>

    <p>libde265 v1.0.8 contains a Heap-use-after-free in intrapred.h when decoding
    file using dec265.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36409">CVE-2021-36409</a>

    <p>There is an Assertion `scaling_list_pred_matrix_id_delta==1' failed at
    sps.cc:925 in libde265 v1.0.8 when decoding file, which allows attackers to
    cause a Denial of Service (DoS) by running the application with a crafted
    file or possibly have unspecified other impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36410">CVE-2021-36410</a>

    <p>A stack-buffer-overflow exists in libde265 v1.0.8 via fallback-motion.cc in
    function put_epel_hv_fallback when running program dec265.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36411">CVE-2021-36411</a>

    <p>An issue has been found in libde265 v1.0.8 due to incorrect access control.
    A SEGV caused by a READ memory access in function derive_boundaryStrength of
    deblock.cc has occurred. The vulnerability causes a segmentation fault and
    application crash, which leads to remote denial of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.0.3-1+deb10u1.</p>

<p>We recommend that you upgrade your libde265 packages.</p>

<p>For the detailed security status of libde265 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libde265">https://security-tracker.debian.org/tracker/libde265</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3240.data"
# $Id: $
