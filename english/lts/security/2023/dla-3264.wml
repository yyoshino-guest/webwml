<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential reflected file download (RFD)
vulnerability in ruby-sinatra, a Ruby library for writing HTTP applications. A
Content-Disposition HTTP header was being incorrectly derived from a
potentially user-supplied filename.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45442">CVE-2022-45442</a>

    <p>Sinatra is a domain-specific language for creating web applications in
    Ruby. An issue was discovered in Sinatra 2.0 before 2.2.3 and 3.0 before
    3.0.4. An application is vulnerable to a reflected file download (RFD)
    attack that sets the Content-Disposition header of a response when the
    filename is derived from user-supplied input. Version 2.2.3 and 3.0.4
    contain patches for this issue.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
2.0.5-4+deb10u2.</p>

<p>We recommend that you upgrade your ruby-sinatra packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3264.data"
# $Id: $
