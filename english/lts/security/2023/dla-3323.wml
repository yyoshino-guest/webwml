<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that in c-ares, an asynchronous name resolver library,
the config_sortlist function is missing checks about the validity of the
input string, which allows a possible arbitrary length stack overflow and
thus may cause a denial of service.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.14.0-1+deb10u2.</p>

<p>We recommend that you upgrade your c-ares packages.</p>

<p>For the detailed security status of c-ares please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/c-ares">https://security-tracker.debian.org/tracker/c-ares</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3323.data"
# $Id: $
