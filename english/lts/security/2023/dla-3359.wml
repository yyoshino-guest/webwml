<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libapache2-mod-auth-mellon, a SAML 2.0 authentication module for Apache,
were reported to have the following vulnerabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13038">CVE-2019-13038</a>

    <p>mod_auth_mellon had an Open Redirect via the login?ReturnTo= substring,
    as demonstrated by omitting the // after http: in the target URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3639">CVE-2021-3639</a>

    <p>mod_auth_mellon did not sanitize logout URLs properly. This issue could
    be used by an attacker to facilitate phishing attacks by tricking users
    into visiting a trusted web application URL that redirects to an external
    and potentially malicious server.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.14.2-1+deb10u1.</p>

<p>We recommend that you upgrade your libapache2-mod-auth-mellon packages.</p>

<p>For the detailed security status of libapache2-mod-auth-mellon please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon">https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3359.data"
# $Id: $
