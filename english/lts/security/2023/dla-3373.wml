<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in Json-smart library.
Json-smart is a performance focused, JSON processor lib written in Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31684">CVE-2021-31684</a>

    <p>A vulnerability was discovered in the indexOf function of
    JSONParserByteArray in JSON Smart versions 1.3 and 2.4
    which causes a denial of service (DOS)
    via for instance a crafted web request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1370">CVE-2023-1370</a>

    <p>A stack overflow was found due to excessive recursion.
    When reaching a ‘[‘ or ‘{‘ character in the JSON input, the code
    parses an array or an object respectively. It was discovered that the
    code does not have any limit to the nesting of such arrays or
    objects. Since the parsing of nested arrays and objects is done
    recursively, nesting too many of them can cause a stack exhaustion
    (stack overflow) and crash the software</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.2-2+deb10u1.</p>

<p>We recommend that you upgrade your json-smart packages.</p>

<p>For the detailed security status of json-smart please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/json-smart">https://security-tracker.debian.org/tracker/json-smart</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3373.data"
# $Id: $
