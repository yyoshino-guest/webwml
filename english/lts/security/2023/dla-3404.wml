<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2196">CVE-2022-2196</a>

    <p>A regression was discovered the KVM implementation for Intel CPUs,
    affecting Spectre v2 mitigation for nested virtualisation.  When
    KVM was used as the L0 hypervisor, an L2 guest could exploit this
    to leak sensitive information from its L1 hypervisor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3424">CVE-2022-3424</a>

    <p>Zheng Wang and Zhuorao Yang reported a flaw in the SGI GRU driver
    which could lead to a use-after-free.  On systems where this driver
    is used, a local user can explit this for denial of service (crash
    or memory corruption) or possibly for privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3707">CVE-2022-3707</a>

    <p>Zheng Wang reported a flaw in the i915 graphics driver's
    virtualisation (GVT-g) support that could lead to a double-free.
    On systems where this feature is used, a guest can exploit this
    for denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4129">CVE-2022-4129</a>

    <p>Haowei Yan reported a race condition in the L2TP protocol
    implementation which could lead to a null pointer dereference.  A
    local user could exploit this for denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4379">CVE-2022-4379</a>

    <p>Xingyuan Mo reported a flaw in the NFSv4.2 inter server to
    server copy implementation which could lead to a use-after-free.</p>

    <p>This feature is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0045">CVE-2023-0045</a>

    <p>Rodrigo Branco and Rafael Correa De Ysasi reported that when a
    user-space task told the kernel to enable Spectre v2 mitigation
    for it, the mitigation was not enabled until the task was next
    rescheduled.  This might be exploitable by a local or remote
    attacker to leak sensitive information from such an application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0458">CVE-2023-0458</a>

    <p>Jordy Zimmer and Alexandra Sandulescu found that getrlimit() and
    related system calls were vulnerable to speculative execution
    attacks such as Spectre v1.  A local user could explot this to
    leak sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0459">CVE-2023-0459</a>

    <p>Jordy Zimmer and Alexandra Sandulescu found a regression in
    Spectre v1 mitigation in the user-copy functions for the amd64
    (64-bit PC) architecture.  Where the CPUs do not implement SMAP or
    it is disabled, a local user could exploit this to leak sensitive
    information from the kernel.  Other architectures may also be
    affected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0461">CVE-2023-0461</a>

    <p><q>slipper</q> reported a flaw in the kernel's support for ULPs (Upper
    Layer Protocols) on top of TCP that can lead to a double-free when
    using kernel TLS sockets.  A local user can exploit this for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p>

    <p>Kernel TLS is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1073">CVE-2023-1073</a>

    <p>Pietro Borrello reported a type confusion flaw in the HID (Human
    Interface Device) subsystem.  An attacker able to insert and
    remove USB devices might be able to use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1074">CVE-2023-1074</a>

    <p>Pietro Borrello reported a type confusion flaw in the SCTP
    protocol implementation which can lead to a memory leak.  A local
    user could exploit this to cause a denial of service (resource
    exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1076">CVE-2023-1076</a>

    <p>Pietro Borrello reported a type confusion flaw in the TUN/TAP
    network driver, which results in all TUN/TAP sockets being marked
    as belonging to user ID 0 (root).  This may allow local users to
    evade local firewall rules based on user ID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1077">CVE-2023-1077</a>

    <p>Pietro Borrello reported a type confusion flaw in the task
    scheduler.  A local user might be able to exploit this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1078">CVE-2023-1078</a>

    <p>Pietro Borrello reported a type confusion flaw in the RDS protocol
    implementation.  A local user could exploit this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1079">CVE-2023-1079</a>

    <p>Pietro Borrello reported a race condition in the hid-asus HID
    driver which could lead to a use-after-free.  An attacker able to
    insert and remove USB devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1118">CVE-2023-1118</a>

    <p>Duoming Zhou reported a race condition in the ene_ir remote
    control driver that can lead to a use-after-free if the driver
    is unbound.  It is not clear what the security impact of this is.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1281">CVE-2023-1281</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2023-1829">CVE-2023-1829</a></p>

    <p><q>valis</q> reported two flaws in the cls_tcindex network traffic
    classifier which could lead to a use-after-free.  A local user can
    exploit these for privilege escalation.  This update removes
    cls_tcindex entirely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1513">CVE-2023-1513</a>

    <p>Xingyuan Mo reported an information leak in the KVM implementation
    for the i386 (32-bit PC) architecture.  A local user could exploit
    this to leak sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1611">CVE-2023-1611</a>

    <p><q>butt3rflyh4ck</q> reported a race condition in the btrfs filesystem
    driver which can lead to a use-after-free.  A local user could
    exploit this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1670">CVE-2023-1670</a>

    <p>Zheng Wang reported a race condition in the xirc2ps_cs network
    driver which can lead to a use-after-free.  An attacker able to
    insert and remove PCMCIA devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1855">CVE-2023-1855</a>

    <p>Zheng Wang reported a race condition in the xgene-hwmon hardware
    monitoring driver that may lead to a use-after-free.  It is not
    clear what the security impact of this is.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1859">CVE-2023-1859</a>

    <p>Zheng Wang reported a race condition in the 9pnet_xen transport
    for the 9P filesystem on Xen, which can lead to a use-after-free.
    On systems where this feature is used, a backend driver in another
    domain can use this to cause a denial of service (crash or memory
    corruption) or possibly to run arbitrary code in the vulnerable
    domain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1872">CVE-2023-1872</a>

    <p>Bing-Jhong Billy Jheng reported a race condition in the io_uring
    subsystem that can lead to a use-after-free.  A local user could
    exploit this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1989">CVE-2023-1989</a>

    <p>Zheng Wang reported a race condition in the btsdio Bluetooth
    adapter driver that can lead to a use-after-free.  An attacker
    able to insert and remove SDIO devices can use this to cause a
    denial of service (crash or memory corruption) or possibly to run
    arbitrary code in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1990">CVE-2023-1990</a>

    <p>Zheng Wang reported a race condition in the st-nci NFC adapter
    driver that can lead to a use-after-free.  It is not clear what
    the security impact of this is.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1998">CVE-2023-1998</a>

    <p>José Oliveira and Rodrigo Branco reported a regression in Spectre
    v2 mitigation for user-space on x86 CPUs supporting IBRS but not
    eIBRS.  This might be exploitable by a local or remote attacker to
    leak sensitive information from a user-space application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2162">CVE-2023-2162</a>

    <p>Mike Christie reported a race condition in the iSCSI TCP transport
    that can lead to a use-after-free.  On systems where this feature
    is used, a local user might be able to use this to cause a denial
    of service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2194">CVE-2023-2194</a>

    <p>Wei Chen reported a potential heap buffer overflow in the
    i2c-xgene-slimpro I²C adapter driver.  A local user with
    permission to access such a device can use this to cause a denial
    of service (crash or memory corruption) and probably for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22998">CVE-2023-22998</a>

    <p>Miaoqian Lin reported an incorrect error check in the virtio-gpu
    GPU driver.  A local user with access to such a device might be
    able to use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23004">CVE-2023-23004</a>

    <p>Miaoqian Lin reported an incorrect error check in the mali-dp GPU
    driver.  A local user with access to such a device might be able
    to use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23559">CVE-2023-23559</a>

    <p>Szymon Heidrich reported incorrect bounds checks in the rndis_wlan
    Wi-Fi driver which may lead to a heap buffer overflow or overread.
    An attacker able to insert and remove USB devices can use this to
    cause a denial of service (crash or memory corruption) or
    information leak, or possibly to run arbitrary code in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25012">CVE-2023-25012</a>

    <p>Pietro Borrello reported a race condition in the hid-bigbenff HID
    driver which could lead to a use-after-free.  An attacker able to
    insert and remove USB devices can use this to cause a denial of
    service (crash or memory corruption) or possibly to run arbitrary
    code in the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26545">CVE-2023-26545</a>

    <p>Lianhui Tang reported a flaw in the MPLS protocol implementation
    that could lead to a double-free.  A local user might be able to
    exploit this to cause a denial of service (crash or memory
    corruption) or possibl for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28328">CVE-2023-28328</a>

    <p>Wei Chen reported a flaw in the az6927 DVB driver that can lead to
    a null pointer dereference.  A local user permitted to access an
    I²C adapter device that this driver creates can use this to cause
    a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28466">CVE-2023-28466</a>

    <p>Hangyu Hua reported a race condition in the kernel TLS socket
    implementation which can lead to a use-after-free or null
    pointer dereference.  A local user can exploit this for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p>

    <p>This feature is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30456">CVE-2023-30456</a>

    <p>Reima ISHII reported a flaw in the KVM implementation for Intel
    CPUs affecting nested virtualisation.  When KVM was used as the L0
    hypervisor, and EPT and/or unrestricted guest mode was disabled,
    it did not prevent an L2 guest from being configured with an
    architecturally invalid protection/paging mode.  A malicious guest
    could exploit this to cause a denial of service (crash).</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.10.178-3~deb10u1.  This update additionally fixes Debian bugs
#989705, #993612, #1022126, and #1031753; and includes many more bug
fixes from stable updates 5.10.163-5.10.178 inclusive.</p>

<p>We recommend that you upgrade your linux-5.10 packages.</p>

<p>For the detailed security status of linux-5.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-5.10">https://security-tracker.debian.org/tracker/linux-5.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3404.data"
# $Id: $
