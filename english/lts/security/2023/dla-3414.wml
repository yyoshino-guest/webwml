<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a local Denial of Service (DoS)
vulnerability in Avahi, a system that facilitates service discovery on a local
network. The avahi-daemon process could have been crashed over the DBus message
bus.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1981">CVE-2023-1981</a>

    <p>avahi-daemon can be crashed via DBus</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
0.7-4+deb10u2.</p>

<p>We recommend that you upgrade your avahi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3414.data"
# $Id: $
