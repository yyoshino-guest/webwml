<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17480">CVE-2018-17480</a>

    <p>Guang Gong discovered an out-of-bounds write issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17481">CVE-2018-17481</a>

    <p>Several use-after-free issues were discovered in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18335">CVE-2018-18335</a>

    <p>A buffer overflow issue was discovered in the skia library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18336">CVE-2018-18336</a>

    <p>Huyna discovered a use-after-free issue in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18337">CVE-2018-18337</a>

    <p>cloudfuzzer discovered a use-after-free issue in blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18338">CVE-2018-18338</a>

    <p>Zhe Jin discovered a buffer overflow issue in the canvas renderer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18339">CVE-2018-18339</a>

    <p>cloudfuzzer discovered a use-after-free issue in the WebAudio
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18340">CVE-2018-18340</a>

    <p>A use-after-free issue was discovered in the MediaRecorder implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18341">CVE-2018-18341</a>

    <p>cloudfuzzer discovered a buffer overflow issue in blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18342">CVE-2018-18342</a>

    <p>Guang Gong discovered an out-of-bounds write issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18343">CVE-2018-18343</a>

    <p>Tran Tien Hung discovered a use-after-free issue in the skia library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18344">CVE-2018-18344</a>

    <p>Jann Horn discovered an error in the Extensions implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18345">CVE-2018-18345</a>

    <p>Masato Kinugawa and Jun Kokatsu discovered an error in the Site Isolation
    feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18346">CVE-2018-18346</a>

    <p>Luan Herrera discovered an error in the user interface.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18347">CVE-2018-18347</a>

    <p>Luan Herrera discovered an error in the Navigation implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18348">CVE-2018-18348</a>

    <p>Ahmed Elsobky discovered an error in the omnibox implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18349">CVE-2018-18349</a>

    <p>David Erceg discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18350">CVE-2018-18350</a>

    <p>Jun Kokatsu discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18351">CVE-2018-18351</a>

    <p>Jun Kokatsu discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18352">CVE-2018-18352</a>

    <p>Jun Kokatsu discovered an error in Media handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18353">CVE-2018-18353</a>

    <p>Wenxu Wu discovered an error in the network authentication implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18354">CVE-2018-18354</a>

    <p>Wenxu Wu discovered an error related to integration with GNOME Shell.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18355">CVE-2018-18355</a>

    <p>evil1m0 discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18356">CVE-2018-18356</a>

    <p>Tran Tien Hung discovered a use-after-free issue in the skia library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18357">CVE-2018-18357</a>

    <p>evil1m0 discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18358">CVE-2018-18358</a>

    <p>Jann Horn discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18359">CVE-2018-18359</a>

    <p>cyrilliu discovered an out-of-bounds read issue in the v8 javascript
    library.</p></li>

</ul>

<p>Several additional security relevant issues are also fixed in this update
that have not yet received CVE identifiers.</p>

<p>For the stable distribution (stretch), these problems have been fixed in
version 71.0.3578.80-1~deb9u1.</p>

<p>We recommend that you upgrade your chromium-browser packages.</p>

<p>For the detailed security status of chromium-browser please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium-browser">\
https://security-tracker.debian.org/tracker/chromium-browser</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4352.data"
# $Id: $
