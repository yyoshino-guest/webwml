<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Dovecot email
server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">CVE-2020-12100</a>

    <p>Receiving mail with deeply nested MIME parts leads to resource
    exhaustion as Dovecot attempts to parse it.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12673">CVE-2020-12673</a>

    <p>Dovecot's NTLM implementation does not correctly check message
    buffer size, which leads to a crash when reading past allocation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12674">CVE-2020-12674</a>

    <p>Dovecot's RPA mechanism implementation accepts zero-length message,
    which leads to assert-crash later on.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:2.3.4.1-5+deb10u3.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>For the detailed security status of dovecot please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dovecot">https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4745.data"
# $Id: $
