<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs reported an authorization bypass
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41974">CVE-2022-41974</a>)
and a symlink attack
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>)
in multipath-tools, a set of tools to drive the Device Mapper multipathing
driver, which may result in local privilege escalation.</p>

<p>Please refer to /usr/share/doc/multipath-tools/NEWS.Debian.gz for
backwards-incompatible changes in this update.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 0.8.5-2+deb11u1.</p>

<p>We recommend that you upgrade your multipath-tools packages.</p>

<p>For the detailed security status of multipath-tools please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/multipath-tools">\
https://security-tracker.debian.org/tracker/multipath-tools</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5366.data"
# $Id: $
