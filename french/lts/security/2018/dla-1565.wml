#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans GlusterFS, un
système de fichiers pour grappe. Des problèmes de dépassement de tampon et de
traversée de répertoires pourraient conduire à une divulgation d'informations,
un déni de service ou à l'exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14651">CVE-2018-14651</a>

<p>Il a été découvert que le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10927">CVE-2018-10927</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10928">CVE-2018-10928</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10929">CVE-2018-10929</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10930">CVE-2018-10930</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10926">CVE-2018-10926</a>
était incomplet. Un attaquant distant authentifié pourrait utiliser un de ces
défauts pour exécuter du code arbitraire, créer des fichiers arbitraires ou
causer un déni de service sur les nœuds de serveur glusterfs à l’aide de liens
symboliques vers des chemins relatifs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14652">CVE-2018-14652</a>

<p>Le système de fichiers Gluster est vulnérable à un dépassement de tampon dans
le traducteur «·fonctions/index·» à l’aide du code de traitement xattr
<q>GF_XATTR_CLRLK_CMD</q> dans la fonction <q>pl_getxattr</q>. Un attaquant
distant authentifié pourrait exploiter cela sur un volume monté pour causer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14653">CVE-2018-14653</a>

<p>Le système de fichiers Gluster est vulnérable à un dépassement de tampon basé
sur le tas dans la fonction <q>__server_getspec</q> à l’aide d’un message RPC
<q>gf_getspec_req</q>. Un attaquant distant authentifié pourrait exploiter cela
pour provoquer un déni de service ou avoir un impact potentiel non précisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14659">CVE-2018-14659</a>

<p>Le système de fichiers Gluster est vulnérable à une attaque par déni de
service lors d’une utilisation de xattr <q>GF_XATTR_IOSTATS_DUMP_KEY</q>. Un
attaquant authentifié pourrait exploiter cela en montant un volume Gluster et en
répétant l’appel «·setxattr(2)·» pour déclencher un vidage d’état et créer un
nombre arbitraire de fichiers dans le répertoire d’exécution du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14661">CVE-2018-14661</a>

<p>Il a été découvert que l’utilisation de la fonction snprintf dans le
traducteur fonction/verrous du serveur de glusterfs était vulnérable à une
attaque de chaîne de formatage. Un attaquant distant authentifié pourrait
utiliser ce défaut pour provoquer un déni de service distant.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.5.2-2+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets glusterfs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1565.data"
# $Id: $
