#use wml::debian::translation-check translation="ceb9e9775384ffb0fb329fce026dfeb293352e3b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été récemment découvertes dans le code de stream-tcp
de l’outil Suricata de détection et prévention d’intrusion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18625">CVE-2019-18625</a>

<p>Il était possible de contourner ou d’éviter n’importe quelle signature basée
sur tcp en simulant une session TCP terminée en utilisant un serveur
malveillant. Après le paquet TCP SYN, il était possible d’injecter un paquet
RST ACK et un paquet FIN ACK avec une mauvaise option de temporalité de TCP.
Le client pouvait ignorer les paquets RST ACK et FIN ACK à cause de cette
mauvaise option pour TCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18792">CVE-2019-18792</a>

<p>Il était possible de contourner ou d’éviter n’importe quelle signature basée
sur tcp en superposant un segment TCP avec un paquet FIN falsifié. Ce paquet
devait être injecté juste avant le paquet PUSH ACK devant être contourné. Le
paquet PUSH ACK (contenant les données) devait être ignoré par Suricata parce
qu’il était superposé au paquet FIN (la séquence et le nombre d’ack sont
identiques dans les deux paquets). Le client pouvait ignorer le paquet FIN
falsifié parce que le drapeau ACK n’était pas défini.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.0.7-2+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets suricata.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2087.data"
# $Id: $
