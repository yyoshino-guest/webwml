#use wml::debian::translation-check translation="874b7a827053e059c3b08e991a12da214544f123" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans libVNC (paquet Debian
libvncserver), une implémentation du protocole client et serveur VNC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20839">CVE-2019-20839</a>

<p>libvncclient/sockets.c dans LibVNCServer possédait un dépassement de tampon
à l'aide d'un nom long de fichier de socket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14397">CVE-2020-14397</a>

<p>libvncserver/rfbregion.c possédait un déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14399">CVE-2020-14399</a>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14400">CVE-2020-14400</a>

<p>Un accès à des données alignées selon les octets (byte-aligned) était
possible à travers des pointeurs uint16_t dans libvncserver/translate.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14401">CVE-2020-14401</a>

<p>libvncserver/scale.c possédait un dépassement d’entier dans pixel_value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14402">CVE-2020-14402</a>

<p>libvncserver/corre.c permettait un accès hors limites à l’aide d’encodages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14403">CVE-2020-14403</a>

<p>libvncserver/hextile.c permettait un accès hors limites à l’aide d’encodages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14404">CVE-2020-14404</a>

<p>libvncserver/rre.c permettait un accès hors limites à l’aide d’encodages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14405">CVE-2020-14405</a>

<p>libvncclient/rfbproto.c ne limitait pas la taille de TextChat.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.9.9+dfsg2-6.1+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvncserver.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2264.data"
# $Id: $
