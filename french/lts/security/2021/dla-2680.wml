#use wml::debian::translation-check translation="82c12926a9bf43d769b05790e41463629f7e145a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jamie Landeg-Jones et Manfred Paul ont découvert une vulnérabilité de
dépassement de tampon dans NGINX, un serveur web et mandataire petit, puissant
et évolutif.</p>

<p>NGINX possédait un dépassement de tampon pour les années excédant quatre
chiffres, comme le montre un fichier avec une date de modification en 1969 qui
provoque un dépassement d'entier (ou une date de modification erronée loin dans
le futur), lorsqu'elles sont rencontrées par le module autoindex.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.10.3-1+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nginx.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nginx, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nginx">\
https://security-tracker.debian.org/tracker/nginx</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2680.data"
# $Id: $
