#use wml::debian::translation-check translation="005b18b85db6c69493f4b8236496721a94963301" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans scilab, notamment dans la
bibliothèque ezXML embarquée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30485">CVE-2021-30485</a>

<p>Gestion incorrecte de la mémoire, conduisant à un déréférencement de
pointeur NULL dans ezxml_internal_dtd()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31229">CVE-2021-31229</a>

<p>Écriture hors limites dans ezxml_internal_dtd(), conduisant à une écriture
hors limites d’une constante d’un octet</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31347">CVE-2021-31347</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-31348">CVE-2021-31348</a>

<p>Traitement incorrect de la mémoire dans ezxml_parse_str(), conduisant
à une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31598">CVE-2021-31598</a>

<p>Écriture hors limites dans ezxml_decode(), conduisant à une corruption
de tas.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.5.2-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets scilab.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de scilab, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/scilab">\
https://security-tracker.debian.org/tracker/scilab</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2705.data"
# $Id: $
