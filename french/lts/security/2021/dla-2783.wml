#use wml::debian::translation-check translation="c096e43b6dfec618a46b1c5a03f256dcfa373b03" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de dépassement d’entier
dans hiredis, une bibliothèque C cliente pour communiquer avec une base de
données Redis. Elle survient lors du traitement et de l’analyse de réponses
« multi-bulk ».</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32765">CVE-2021-32765</a>

<p>Hiredis est une bibliothèque C cliente minimaliste pour la base de données
Redis. Dans les versions affectées, Hiredis est vulnérable à un dépassement
d'entier si des données malveillantes ou contrefaites « RESP » du protocole
« mult-bulk » sont fournies. Lors de l’analyse de réponses « multi-bulk »
(genre tableau), hiredis échoue à vérifier si  « count * sizeof(redisReply*) »
peut être représenté dans « SIZE_MAX ». S’il ne peut pas, et si l’appel
« calloc() » ne réalise pas lui-même cette vérification, cela conduirait à
une courte allocation et un dépassement de tampon subséquent. Les utilisateurs
qui ne peuvent pas mettre à jour peuvent régler l’option de contexte
[maxelements] (https://github.com/redis/hiredis#reader-max-array-elements) à
une valeur suffisamment petite pour qu’aucun débordement ne soit possible.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.13.3-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hiredis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2783.data"
# $Id: $
