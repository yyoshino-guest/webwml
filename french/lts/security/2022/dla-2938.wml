#use wml::debian::translation-check translation="ee04ac724b89f68d062f597746abe99777dc4488" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu'il y avait un problème dans Twisted, un cadriciel
réseau en Python où les implémentations client et serveur SSH pouvaient
accepter une quantité de données infinie pour l'identificateur de version
SSH du pair et qu'un tampon utilisait toute la mémoire disponible.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21716">CVE-2022-21716</a>

<p>CVE-2022-21716 : Twisted est un cadriciel basé sur les événements pour
les applications internet, prenant en charge Python à partir de 3.6. Avant
la version 22.2.0, les implémentations client et serveur SSH de Twisted
sont capables d'accepter une quantité de données infinie pour
l'identificateur de version SSH du pair. Cela aboutit à ce qu'un tampon
utilise toute la mémoire disponible. L'attaque est aussi simple que « nc
-rv localhost 22 &lt;/dev/zero ». Un correctif est disponible dans la
version 22.2.0. Il n'y a pas actuellement de palliatif connu.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
16.6.0-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets twisted.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2938.data"
# $Id: $
