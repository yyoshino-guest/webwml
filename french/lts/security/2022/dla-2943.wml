#use wml::debian::translation-check translation="beeb624fdd720da9958fc4d59d51820cc22c15c7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>ruby-sidekiq, un outil de traitement en tâche de fond simple et efficace
pour Ruby, présente les deux vulnérabilités suivantes :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30151">CVE-2021-30151</a>

<p>Sidekiq permet une attaque par script intersite (XSS) à l'aide du nom de
la file d'attente de la fonctionnalité live-poll quand Internet Explorer
est utilisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23837">CVE-2022-23837</a>

<p>Dans api.rb de Sidekiq, il n'y pas de limite de nombre de jours lors
d'une requête de statistique pour le graphique. Cela surcharge le système,
affectant l'interface utilisateur Web, et le rend inaccessible aux
utilisateurs.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.2.3+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-sidekiq.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-sidekiq,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-sidekiq">\
https://security-tracker.debian.org/tracker/ruby-sidekiq</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2943.data"
# $Id: $
