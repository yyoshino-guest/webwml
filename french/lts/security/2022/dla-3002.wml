#use wml::debian::translation-check translation="12e1be1439ebc19dac4483216176e2ba722eeb77" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un problème dans Adminer, l'outil de base de données basé sur
le web, grâce auquel un attaquant pouvait réaliser la lecture d'un fichier
arbitraire sur le serveur distant en demandant à Adminer de se connecter à
une base de données MySQL distante contrefaite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43008">CVE-2021-43008</a>

<p>Un contrôle d'accès incorrect dans Adminer versions 1.12.0 à 4.6.2
(corrigé dans la version 4.6.3) permet à un attaquant de réaliser la
lecture d'un fichier arbitraire sur le serveur distant en demandant à
Adminer de se connecter à une base de données MySQL distante.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.2.5-3+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets adminer.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3002.data"
# $Id: $
