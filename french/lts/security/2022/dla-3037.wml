#use wml::debian::translation-check translation="89d31e1cc9460f0f952b54d0785cc90649b8eb72" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs dépassements d'entiers ont été découverts dans TurboJPEG, une
bibliothèque de gestion d'images JPEG, qui peuvent conduire à un déni de
service (plantage d'application) lors d'une tentative de compression ou de
décompression d'images gigapixel avec l'API TurboJPEG.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:1.5.1-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjpeg-turbo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libjpeg-turbo,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libjpeg-turbo">\
https://security-tracker.debian.org/tracker/libjpeg-turbo</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3037.data"
# $Id: $
