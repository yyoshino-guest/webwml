#use wml::debian::translation-check translation="af1283a2b7b702837d3ac71c1ef0250f870e005c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait deux problèmes dans connman, un démon de gestion de connexion
internet pour périphériques embarqués :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32292">CVE-2022-32292</a>

<p>Dans ConnMan jusqu'à la version 1.41, des attaquants distants capables
d'envoyer des requêtes HTTP au composant gweb pouvaient exploiter un
dépassement de tas dans received_data pour exécuter du code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32292">CVE-2022-32292</a>

<p>Dans ConnMan jusqu'à la version 1.41, une attaque de type <q>homme du
milieu</q> à l'encontre d'une requête HTTP WISPR pouvait être utilisée pour
déclencher une utilisation de mémoire après libération dans le traitement
de WISPR, menant à des plantages ou à l'exécution de code.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 1.36-2.1~deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets connman.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3105.data"
# $Id: $
