#use wml::debian::translation-check translation="e1c06daa5e906ac14381c26b188b2ded9c79da65" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs défauts ont été découverts dans jackson-databind, une bibliothèque
JSON puissante et rapide pour Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36518">CVE-2020-36518</a>

<p>Exception Java StackOverflow et déni de service à l'aide d'une grande
profondeur d’objets imbriqués.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42003">CVE-2022-42003</a>

<p>Dans FasterXML un épuisement de ressources de jackson-databind peut se
produire à cause d’une absence de vérification dans les désérialiseurs de valeur
de primitive pour éviter une imbrication profonde de tableau d’enveloppes quand
la caractéristique UNWRAP_SINGLE_VALUE_ARRAYS est activée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42004">CVE-2022-42004</a>

<p>Dans FasterXML un épuisement de ressources de jackson-databind peut se
produire à cause d’une absence de vérification dans
BeanDeserializerBase.deserializeFromArray pour éviter une imbrication profonde
de tableau. Une application est vulnérable seulement avec certains choix
personnels pour la désérialisation.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2.9.8-3+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jackson-databind,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3207.data"
# $Id: $
