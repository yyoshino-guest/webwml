#use wml::debian::translation-check translation="7dde3a7127a20b2ae5d58f63ef1bec4f2f899471" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que joblib ne nettoyait pas complètement les arguments
de pre_dispatch, permettant l’exécution de code arbitraire. La précédente
tentative à travers la DLA-3193-1 était incomplète.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.13.0-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets joblib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de joblib,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/joblib">\
https://security-tracker.debian.org/tracker/joblib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3193-2.data"
# $Id: $
