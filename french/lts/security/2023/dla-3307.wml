#use wml::debian::translation-check translation="43529e55b074ed02c5ca2027d17e9ecd95678480" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’environnement d’exécution
Java OpenJDK, qui pouvaient aboutir à un déni de service ou une usurpation.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 11.0.18+10-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjdk-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjdk-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjdk-11">\
https://security-tracker.debian.org/tracker/openjdk-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3307.data"
# $Id: $
