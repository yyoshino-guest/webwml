#use wml::debian::template title="Sicherheits-Informationen" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c99908e0effa46ed240da78ee4a1b5ea8e13fcd4"
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Ihr Debian-System sicher halten</a></li>
<li><a href="#DSAS">Letzte Ankündigungen</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian nimmt die Sicherheit sehr ernst. Wir behandeln alle
         Sicherheitsprobleme, die uns bekannt gemacht werden, und stellen sicher,
         dass diese innerhalb eines angemessenen Zeitrahmens korrigiert werden.</p>
</aside>

<p>
Die Erfahrung zeigt, dass <q>Sicherheit durch
Verschleierung</q> (englisch: <em>security through obscurity</em>)
nicht funktioniert. Die Offenlegung erlaubt es, bessere und schnellere
Lösungen für Sicherheitsprobleme zu finden.
In diesem Sinne zeigt diese Seite den Stand des Debian-Projekts in
Bezug auf verschiedene bekannte Sicherheitsmängel, die potentiell auch
das Debian-Betriebssystem betreffen könnten.
</p>

<p>
Das Debian-Projekt koordiniert viele Sicherheits-Ankündigungen mit anderen
Anbietern freier Software, dadurch werden diese Ankündigungen am gleichen Tag
veröffentlicht, an dem auch die Verwundbarkeit bekannt gegeben wird.
</p>
  
# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>
Debian beteiligt sich zusätzlich an Bemühungen, die Behandlung von
Sicherheitsproblemen zu standardisieren:
</p>

<ul>
  <li>Die <a href="#DSAS">Debian-Sicherheitsankündigungen</a> sind <a href="cve-compatibility">CVE-kompatibel</a> (siehe die <a href="crossreferences">Querverweise</a>).</li>
  <li>Debian ist im Vorstand des <a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a>-Projekts vertreten.</li>
</ul>

<h2><a id="keeping-secure">Ihr Debian-System sicher halten</a></h2>


<p>
Um die neuesten Debian-Sicherheitsankündigungen zu erhalten, abonnieren Sie die Mailingliste
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Zusätzlich dazu können Sie <a href="https://packages.debian.org/stable/admin/apt">APT</a> verwenden,
um ganz einfach die neuesten Sicherheitsaktualisierungen zu bekommen. Um das System in Bezug auf
Sicherheits-Patches aktuell zu halten, fügen Sie bitte folgende Zeile zu Ihrer
<code>/etc/apt/sources.list</code>-Datei hinzu:
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free
</pre>

<p>
Nach dem Einpflegen dieser Änderungen führen Sie folgende Befehle aus, um alle ausstehenden Aktualisierungen herunterzuladen
und zu installieren:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
Das security-Archiv ist mit dem normalen Debian Archiv-<a href="https://ftp-master.debian.org/keys.html">Signaturschlüssel</a>
signiert.
</p>

<p>
Für weitere Informationen bezüglich Sicherheitsfragen in Debian konsultieren Sie bitte
unsere FAQ und Dokumentation:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Sicherheits-FAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Securing-Debian-Handbuch</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Letzte Ankündigungen</a></h2>

<p>Diese Webseiten enthalten ein verdichtetes Archiv von
Sicherheitsankündigungen, die auf der Mailingliste
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a> veröffentlicht wurden.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<p>
Die neuesten Debian-Sicherheitsankündigungen sind ebenfalls im <a href="dsa">RDF-Format</a> verfügbar.
Wir bieten auch eine zweite, <a href="dsa-long">etwas ausführlichere Version</a> dieser Dateien an,
die den ersten Absatz der entsprechenden Ankündigungen enthält. Somit können Sie schnell sehen, worum es
in der Ankündigung in etwa geht.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Ältere Sicherheitsankündigungen sind ebenfalls verfügbar:
<:= get_past_sec_list(); :>

<p>Die Debian-Distributionen sind nicht für alle Sicherheitsprobleme
anfällig. Der <a href="https://security-tracker.debian.org/">Debian Security Tracker</a> 
sammelt alle Informationen über den Verwundbarkeitsstatus von Debian-Paketen. Diese
können nach dem CVE-Namen oder dem Paketnamen durchsucht werden.</p>
