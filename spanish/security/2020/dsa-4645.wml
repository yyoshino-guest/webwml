#use wml::debian::translation-check translation="9f255d7246e224b0653339ff32337f4de3f80b46"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20503">CVE-2019-20503</a>

   <p>Natalie Silvanovich descubrió un problema de lectura fuera de límites en la biblioteca
   usrsctp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6422">CVE-2020-6422</a>

    <p>David Manouchehri descubrió un problema de «uso tras liberar» en la implementación
    de WebGL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6424">CVE-2020-6424</a>

    <p>Sergei Glazunov descubrió un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6425">CVE-2020-6425</a>

    <p>Sergei Glazunov descubrió un error de imposición de reglas relacionado con
    extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6426">CVE-2020-6426</a>

    <p>Avihay Cohen descubrió un error de implementación en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6427">CVE-2020-6427</a>

    <p>Man Yue Mo descubrió un problema de «uso tras liberar» en la implementación de audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6428">CVE-2020-6428</a>

    <p>Man Yue Mo descubrió un problema de «uso tras liberar» en la implementación de audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6429">CVE-2020-6429</a>

    <p>Man Yue Mo descubrió un problema de «uso tras liberar» en la implementación de audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6449">CVE-2020-6449</a>

    <p>Man Yue Mo descubrió un problema de «uso tras liberar» en la implementación de audio.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), el soporte de seguridad para chromium se
ha discontinuado.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 80.0.3987.149-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4645.data"
