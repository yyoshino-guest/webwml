#use wml::debian::translation-check translation="ae06255bde57d831150a61d8cba709fe69cdbd83"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fuga de
información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

    <p>Paulo Bonzini descubrió que la implementación de KVM para procesadores
    Intel no realizaba correctamente la emulación de instrucciones para huéspedes
    L2 cuando estaba habilitada la virtualización anidada. Esto podía permitir
    que un huésped L2 provocara elevación de privilegios, denegación de servicio
    o fugas de información en el huésped L1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

    <p>Al Viro descubrió una vulnerabilidad de «uso tras liberar» en la capa
    VFS. Esto permitía que usuarios locales provocaran denegación de servicio
    (caída) o que obtuvieran información sensible de la memoria del núcleo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

    <p>Se descubrió que el controlador vhost_net no validaba
    correctamente el tipo de sockets definidos como backends. Un usuario local
    autorizado a acceder a /dev/vhost-net podía usar esto para provocar corrupción
    de pila a través de llamadas al sistema manipuladas, dando lugar a denegación de
    servicio (caída) o, posiblemente, a elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

    <p>Entropy Moe informó de que el sistema de archivos en memoria compartida (tmpfs)
    no trataba correctamente una opción de montaje <q>mpol</q> especificando una lista
    de nodos vacía, lo que daba lugar a escritura fuera de límites en la pila. Si los espacios
    de nombres de usuario estaban habilitados, un usuario local podía utilizar esto para provocar
    denegación de servicio (caída) o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11884">CVE-2020-11884</a>

    <p>Al Viro informó de una condición de carrera en el código de gestión de memoria para
    el IBM Z (arquitectura s390x) que podía dar lugar a ejecución de
    código del núcleo desde el espacio de direcciones del usuario. Un usuario local podía
    usar esto para elevación de privilegios.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.19.98-1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4667.data"
